#!/bin/bash
echo "-----------------------------------------------"
echo "Down no sonarqube"
echo ""
docker-compose -f ./sonarqube/docker-compose.yml down

echo "-----------------------------------------------"
echo "Down no gitlab"
echo ""
docker-compose -f ./gitlab/docker-compose.yml down
echo "-----------------------------------------------"
echo "Down no jenkins"
echo ""
docker-compose -f ./jenkins/docker-compose.yml down
echo "-----------------------------------------------"
echo "Down no Proxy"
echo ""
docker-compose -f ./nginx-proxy/docker-compose.yml down
echo "-----------------------------------------------"
