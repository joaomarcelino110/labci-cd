#!/bin/bash
echo "-----------------------------------------------"
echo ""
echo "Levantando Proxy"
docker-compose -f ./nginx-proxy/docker-compose.yml up -d
sleep 3
# JENKINS=$(docker container inspect jenkins-maven | jq -r '.[].NetworkSettings.Networks.jenkins.IPAddress')
echo ""
echo "-----------------------------------------------"
echo ""

echo "Levantando Jenkins"
docker-compose -f ./jenkins/docker-compose.yml up -d
sleep 3
# JENKINS=$(docker container inspect jenkins-maven | jq -r '.[].NetworkSettings.Networks.jenkins.IPAddress')
echo "Acesse container Jenkins: http://jenkins.desafio.local"
echo ""
echo "-----------------------------------------------"
echo ""

echo "Levantando Sonarqube"
# sudo sh ./sonarqube/sonar.sh
docker-compose -f ./sonarqube/docker-compose.yml up -d
sleep 3
# SONAR=$(docker container inspect sonarqube | jq -r '.[].NetworkSettings.Networks.jenkins.IPAddress')
echo "Acesse container SonarQube: http://sonarqube.desafio.local"
echo ""
echo "-----------------------------------------------"

echo "Levantando Gitlab"
docker-compose -f ./gitlab/docker-compose.yml up -d
sleep 10
# GITLAB=$(docker container inspect gitlab-ce | jq -r '.[].NetworkSettings.Networks.jenkins.IPAddress')
echo "Acesse container GitLab: http://gitlab.desafio.local"
echo ""
echo "-----------------------------------------------"
echo ""